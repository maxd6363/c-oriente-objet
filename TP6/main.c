#include <math.h>
#include <stdio.h>

void test1() {
	float a = 0.7;
	float b = 0.5;
	if (a < 0.7) {
		if (b < 0.5)
			printf("2 conditions are right");
		else
			printf("only 1 is right");
	} else
		printf("0 conditions are right");
}

void test1bis() {
	double a = 0.7;
	double b = 0.5;
	if (a < 0.7) {
		if (b < 0.5)
			printf("2 conditions are right");
		else
			printf("only 1 is right");
	} else
		printf("0 conditions are right");
}

void test2() {
	double a = 10;
	double b = sqrt(10);

	printf("%e\n", 10 - (b * b));
}

#define EPSILON 1e-10

void test2bis() {
	double a = 10;
	double b = sqrt(10);

	if (fabs(10 - (b * b)) < EPSILON)
		printf("OK\n");
}

double relativeErrpr(double result, double expected) {
	return fabs((result - expected) / expected);
}

void test3() {
	double result1 = 99.5, result2 = 1.5;
	double expected1 = 100, expected2 = 1;

	printf("%lf -> %lf (%e)\n", expected1, result1, relativeErrpr(expected1, result1));
	printf("%lf -> %lf (%e)\n", result2, expected2, relativeErrpr(result2, expected2));
}

void test4() {
	float f = 0.0000001f;
	float sum = 0;

	for (int i = 0; i < 10000000; ++i)
		sum += f;

	float product = f * 10000000;

	printf("sum : %1.15f, prod : %1.15f\n", sum, product);
	printf("%lf -> %lf (%e)\n", sum, 1., relativeErrpr(sum, 1));
}

void test5() {
	double a = 1000;
	double b = a + 0.5 * __DBL_EPSILON__;

	printf("%e == %e\n", a, b);
	printf("%s\n", a == b ? "true" : "false");
}

double logFactorial(int n) {
	double sum = 0.0;
	for (int i = 2; i <= n; ++i)
		sum += log((double)i);
	return sum;
}

double factorial(int n) {
	double sum = 1;
	for (int i = 1; i < n + 1; i++)
		sum *= i;
	return sum;
}

void test6() {
	double result = exp(logFactorial(200) - logFactorial(190) - logFactorial(10));
	double dummyResult = factorial(200) / factorial(190) / factorial(10);
	printf("with log : %lf\n", result);
	printf("without log : %lf\n", dummyResult);
}

void test7() {
	double x = __DBL_MAX__;
	printf("%s\n", ((x * 2) / 2 == x) ? "true" : "false");
	printf("%lf\n",x * 2 );
	printf("%lf\n", 0. / 0 );
	printf("%lf\n", sqrt(-100) );
}

int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	printf("Test 1 \n");
	test1();
	printf("\n\n");
	printf("Test 1 bis \n");
	test1bis();
	printf("\n\n");
	printf("Test 2\n");
	test2();
	printf("\n\n");
	printf("Test 2bis\n");
	test2bis();
	printf("\n\n");
	printf("Test 3\n");
	test3();
	printf("\n\n");
	printf("Test 4\n");
	test4();
	printf("\n\n");
	printf("Test 5\n");
	test5();
	printf("\n\n");
	printf("Test 6\n");
	test6();
	printf("\n\n");
	printf("Test 7\n");
	test7();
	printf("\n\n");

	return 0;
}
