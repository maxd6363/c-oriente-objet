#include "TP1.h"
#include "mt19937ar.h"
#include <stdio.h>

extern long tabMTSize;
extern double tabMT[];

extern long tabMTSizeBin;
extern double tabMTBin[];

/**
 * @brief Simule la valeur de PI avec Monte Carlo
 *
 * @param numberOfPoints Nombre de point à tirer
 * @return double Valeur de PI simulé
 */
double simulationPiMC(long numberOfPoints) {
	long pointsInsideDisk = 0;
	double x;
	double y;

	for (long i = 0; i < numberOfPoints; i++) {
		// x = genrand_real1();
		// y = genrand_real1();

		x = genrand_real1_lookup();
		y = genrand_real1_lookup();

		if (x * x + y * y < 1) {
			pointsInsideDisk++;
		}
	}
	return 4.0 * ((double)pointsInsideDisk / numberOfPoints);
}

double genrand_real1_lookup(void) {
	static long index = 0;
	index++;
	return tabMT[index - 1];
}

double genrand_real1_lookup_bin(void) {
	static long index = 0;
	index++;
	return tabMTBin[index - 1];
}

long long timeval_diff(struct timeval *difference, struct timeval *end_time, struct timeval *start_time) {
	struct timeval temp_diff;

	if (difference == NULL) {
		difference = &temp_diff;
	}

	difference->tv_sec = end_time->tv_sec - start_time->tv_sec;
	difference->tv_usec = end_time->tv_usec - start_time->tv_usec;

	/* Using while instead of if below makes the code slightly more robust. */

	while (difference->tv_usec < 0) {
		difference->tv_usec += 1000000;
		difference->tv_sec -= 1;
	}

	return 1000000LL * difference->tv_sec +
		   difference->tv_usec;
}