#pragma once
#include <stddef.h>

typedef struct ObjetGraphique ObjetGraphique;
typedef enum OG_t { RECTANGLE = 0,CERCLE = 1, OBJETGRAPHIQUE = 2 } OG_t;

struct ObjetGraphique {
	struct MetaObjetGraphique *class;
	int x;
	int y;
	OG_t type;
};
