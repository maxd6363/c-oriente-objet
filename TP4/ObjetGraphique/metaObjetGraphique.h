#pragma once
#include "objetGraphique.h"

#define OG_NBCLASSES 2

typedef struct ObjetGraphique *ObjetGraphiqueThis;
typedef struct MetaObjetGraphique MetaObjetGraphique;

struct MetaObjetGraphique {
	/* méthodes d’instances */
	void (*setX)(int, ObjetGraphiqueThis);
	void (*setY)(int, ObjetGraphiqueThis);
	int (*getX)(ObjetGraphiqueThis);
	int (*getY)(ObjetGraphiqueThis);

	void (*TVMeffacer[OG_NBCLASSES])(ObjetGraphiqueThis);
	void (*TVMafficher[OG_NBCLASSES])(ObjetGraphiqueThis);
	void (*TVMdeplacer[OG_NBCLASSES])(ObjetGraphiqueThis, int, int);
	int (*TVMgetCentreX[OG_NBCLASSES])(ObjetGraphiqueThis);
	int (*TVMgetCentreY[OG_NBCLASSES])(ObjetGraphiqueThis);

	void (*effacer)(ObjetGraphiqueThis);
	void (*afficher)(ObjetGraphiqueThis);
	void (*deplacer)(ObjetGraphiqueThis, int, int);
	int (*getCentreX)(ObjetGraphiqueThis);
	int (*getCentreY)(ObjetGraphiqueThis);

	int NbObjetGraphique;
	int (*GetNbObjetGraphique)(void);
	void (*ConstructeurObjetGraphique)(ObjetGraphiqueThis);
};

void setX(int in_x, ObjetGraphiqueThis this);
void setY(int in_y, ObjetGraphiqueThis this);
int getX(ObjetGraphiqueThis this);
int getY(ObjetGraphiqueThis this);
void effacer(ObjetGraphiqueThis this);
void afficher(ObjetGraphiqueThis this);
void deplacer(ObjetGraphiqueThis this, int in_dx, int in_dy);
int getCentreX(ObjetGraphiqueThis this);
int getCentreY(ObjetGraphiqueThis this);
int GetNbObjetGraphique(void);
void ConstructeurObjetGraphique(ObjetGraphiqueThis this);
