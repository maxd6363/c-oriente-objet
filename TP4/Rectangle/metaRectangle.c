#include "metaRectangle.h"
#include <stdio.h>

extern MetaObjetGraphique ObjetGraphiqueClass;

MetaRectangle RectangleClass = {&ObjetGraphiqueClass, setHauteur, setLargeur, getHauteur, getLargeur, ConstructeurRectangle};

void setHauteur(int in_hauteur, RectangleThis this) {
	this->hauteur = in_hauteur;
}

void setLargeur(int in_largeur, RectangleThis this) {
	this->largeur = in_largeur;
}

int getHauteur(RectangleThis this) {
	return this->hauteur;
}

int getLargeur(RectangleThis this) {
	return this->largeur;
}

void ConstructeurRectangle(RectangleThis this) {
	ObjetGraphiqueClass.ConstructeurObjetGraphique(&(this->super));
	this->super.type = RECTANGLE;
	this->class = &RectangleClass;
	this->largeur = 0;
	this->hauteur = 0;
}
