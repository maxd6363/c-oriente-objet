#pragma once
#include "../ObjetGraphique/metaObjetGraphique.h"
#include "metaCercle.h"

typedef struct Cercle Cercle;

struct Cercle {
	struct ObjetGraphique super;
	struct MetaCercle *class;
	int rayon;
};

void effacerCercle(ObjetGraphiqueThis this);
void afficherCercle(ObjetGraphiqueThis this);
void deplacerCercle(ObjetGraphiqueThis this, int in_dx, int in_dy);
int getCentreXCercle(ObjetGraphiqueThis this);
int getCentreYCercle(ObjetGraphiqueThis this);