#pragma once
#include "../ObjetGraphique/metaObjetGraphique.h"
#include "cercle.h"

typedef struct Cercle *CercleThis;
typedef struct MetaCercle MetaCercle;

struct MetaCercle {
	struct MetaObjetGraphique *superMetaClasse;
	void (*setRayon)(int, CercleThis);
	int (*getRayon)(CercleThis);
	void (*ConstructeurCercle)(CercleThis);
};

void setRayon(int, CercleThis);
int getRayon(CercleThis);
void ConstructeurCercle(CercleThis);
