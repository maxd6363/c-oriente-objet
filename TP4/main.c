#include <stdio.h>
#include <stdlib.h>

#include "Cercle/metaCercle.h"
#include "ObjetGraphique/metaObjetGraphique.h"
#include "Rectangle/metaRectangle.h"

extern MetaObjetGraphique ObjetGraphiqueClass;
extern MetaCercle CercleClass;
extern MetaRectangle RectangleClass;

void testOG(void) {
	ObjetGraphique obj;
	ObjetGraphiqueClass.ConstructeurObjetGraphique(&obj);
	obj.class->setX(69, &obj);
	obj.class->setY(55, &obj);

	printf("Objet Graphique : %d %d\n", obj.class->getX(&obj), obj.class->getY(&obj));
	printf("Nombre d'instance : %d\n", ObjetGraphiqueClass.GetNbObjetGraphique());
	printf("\n\n");
}

void testCercle(void) {
	Cercle obj;
	CercleClass.ConstructeurCercle(&obj);
	obj.class->setRayon(69, &obj);

	obj.super.class->afficher((ObjetGraphiqueThis)&obj);
	obj.super.class->deplacer((ObjetGraphiqueThis)&obj, 16, 13);
	obj.super.class->afficher((ObjetGraphiqueThis)&obj);

	printf("Center : {%d %d}\n", obj.super.class->getCentreX((ObjetGraphiqueThis)&obj), obj.super.class->getCentreY((ObjetGraphiqueThis)&obj));

	obj.super.class->effacer((ObjetGraphiqueThis)&obj);
	obj.super.class->afficher((ObjetGraphiqueThis)&obj);

	printf("Nombre d'instance : %d\n", ObjetGraphiqueClass.GetNbObjetGraphique());
	printf("\n\n");
}

void testRectangle(void) {
	Rectangle obj;
	RectangleClass.ConstructeurRectangle(&obj);
	obj.class->setHauteur(69, &obj);
	obj.class->setLargeur(12, &obj);

	obj.super.class->afficher((ObjetGraphiqueThis)&obj);
	obj.super.class->deplacer((ObjetGraphiqueThis)&obj, 16, 13);
	obj.super.class->afficher((ObjetGraphiqueThis)&obj);

	printf("Center : {%d %d}\n", obj.super.class->getCentreX((ObjetGraphiqueThis)&obj), obj.super.class->getCentreY((ObjetGraphiqueThis)&obj));

	obj.super.class->effacer((ObjetGraphiqueThis)&obj);
	obj.super.class->afficher((ObjetGraphiqueThis)&obj);

	printf("Nombre d'instance : %d\n", ObjetGraphiqueClass.GetNbObjetGraphique());
	printf("\n\n");
}

int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	testOG();
	testCercle();
	testRectangle();

	return 0;
}
