#include "pile.h"
//#include <stdio.h>

DECLARER_META_PILE(int)
DECLARER_META_PILE(float)

DECLARER_PILE(int)
DECLARER_PILE(float)

IMPLEMENTER_PILE(int)
IMPLEMENTER_PILE(float)

IMPLEMENTER_META_PILE(int)
IMPLEMENTER_META_PILE(float)

void testPile(void) {
	Pileint_t pi;
	Pilefloat_t pf;

	LaMetaPileint.construire(&pi);
    pi.class->empiler(5, &pi);
	printf("Int : %d\n", pi.class->sommet(&pi));
	printf("Int : %s\n", pi.class->estVide(&pi) ? "Vide" : "Pas vide");
	printf("Int : %d\n", pi.class->depiler(&pi));
	printf("Int : %s\n", pi.class->estVide(&pi) ? "Vide" : "Pas vide");

	LaMetaPilefloat.construire(&pf);
	pf.class->empiler(5.69f, &pf);
	printf("Float : %f\n", pf.class->sommet(&pf));
	printf("Float : %s\n", pf.class->estVide(&pf) ? "Vide" : "Pas vide");
	printf("Float : %f\n", pf.class->depiler(&pf));
	printf("Float : %s\n", pf.class->estVide(&pf) ? "Vide" : "Pas vide");
}

int main(int argc, char const *argv[]) {
	(void)argc;
	(void)argv;

	testPile();

	return 0;
}
