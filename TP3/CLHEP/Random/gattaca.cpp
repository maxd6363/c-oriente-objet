#include "CLHEP/Random/MTwistEngine.h" // Entête pour MT dans la bibliothèque CLHEP

#include <cstring>
#include <fcntl.h>
#include <iostream>
#include <limits.h>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cmath>

inline char getNucleique(double value) {
	if (value < 0.25)
		return 'A';
	if (value < 0.5)
		return 'C';
	if (value < 0.75)
		return 'G';
	return 'T';
}

inline char getLetter(double value) {
	return 'A' + ('Z' - 'A' + 1) * value;
}

inline bool checkWord(char const *word, char const *toFind) {
	return strcmp(word, toFind) == 0;
}

void displayStats(long long const reps[], int nbRepWanted){
	long totalForAverage = 0;
	double totalForVariance = 0;
	long min = LONG_MAX;
	long max = 0;

	for (size_t i = 0; i < nbRepWanted; i++){
		totalForAverage+=reps[i];
		if(reps[i] < min) min = reps[i];
		if(reps[i] > max) max = reps[i];
	}
	double average = totalForAverage / (double)nbRepWanted;

	std::cout << "Min : " << min << std::endl;
	std::cout << "Max : " << max << std::endl;
	std::cout << "Moyenne : " << average << std::endl;
	
	for (size_t i = 0; i < nbRepWanted; i++)
		totalForVariance+=(reps[i] - average)*(reps[i] - average);
	
	double variance = totalForVariance / nbRepWanted;
	double deviation = std::sqrt(variance);
	
	std::cout << "Variance : " << variance << std::endl;
	std::cout << "Écart-type : " << deviation << std::endl;

	double lowerConfidence95 = average - 1.96 * std::sqrt(variance / nbRepWanted);
	double upperConfidence95 = average + 1.96 * std::sqrt(variance / nbRepWanted);

	double lowerConfidence99 = average - 2.58 * std::sqrt(variance / nbRepWanted);
	double upperConfidence99 = average + 2.58 * std::sqrt(variance / nbRepWanted);

	std::cout << "Intervalle de confiance à 95% : [" << lowerConfidence95 << ";" << upperConfidence95 << "]" << std::endl;
	std::cout << "Intervalle de confiance à 99% : [" << lowerConfidence99 << ";" << upperConfidence99 << "]" << std::endl;

	std::cout << "\n\n";
}

int main(int argc, char **argv) {
	if(argc != 2){
		std::cerr << "Arguments invalides" << std::endl;
		//return -1;
	}

	char const *wordToFind = "ISIMA";
	
	int nbRepWanted = 400;
	//size_t const wordSize = sizeof(wordToFind) - 1;
	size_t const wordSize = sizeof("ISIMA") - 1;
	
	CLHEP::MTwistEngine *mtRng = new CLHEP::MTwistEngine();
	if(argc == 2)
		mtRng->restoreStatus(argv[1]);

	char array[wordSize][wordSize + 1] = {{0}};
	long long reps[nbRepWanted];
	long long currentRep = 0;
	long long lastFound = 0;

	for (long long i = 0;; i++) {
		//char current = getNucleique(mtRng->flat());
		char current = getLetter(mtRng->flat());
		for (size_t j = 0; j < wordSize; j++) {
			array[j][((i % wordSize) + j) % wordSize] = current;
			if (checkWord(array[j], wordToFind)) {
				reps[currentRep] = i - lastFound;
				lastFound = i;
				currentRep++;
				memset(array, 0, wordSize * (wordSize + 1));
				std::cout << "Found at " << i << std::endl;
			}
		}
		if (currentRep == nbRepWanted)
			break;
	}
	
	displayStats(reps, nbRepWanted);

	delete mtRng;
	return 0;
}
